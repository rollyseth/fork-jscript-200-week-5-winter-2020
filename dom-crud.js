// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
const link= document.createElement('a');
link.innerText='Buy Now';
link.id='cta'; 
const main=document.querySelector('main');
main.appendChild(link);
// Access (read) the data-color attribute of the <img>,
// log to the console
const imgEl=document.getElementsByTagName('img')[0];
console.log(imgEl.dataset.color);

// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"

const li3=document.getElementsByTagName('li')[2];
li3.className='highlight';

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
const pElement=document.getElementsByTagName('p')[0];
main.removeChild(pElement); 