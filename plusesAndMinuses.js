let count=0;
const countEl= document.getElementById('count');
// When a user clicks the + element, the count should increase by 1 on screen.
const plusEl= document.getElementById('plus');
plusEl.addEventListener('click',()=>{
    count++;
    countEl.innerText=count;
})
// When a user clicks the – element, the count should decrease by 1 on screen.
const minusEl= document.getElementById('minus');
minusEl.addEventListener('click',()=>{
    count--;
    countEl.innerText=count;
})